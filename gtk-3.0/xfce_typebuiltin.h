
/* Generated data (by glib-mkenums) */


#include <gtk/gtk.h>

G_BEGIN_DECLS

/* enumerations from "./xfce_style_types.h" */
GType xfce_grip_style_get_type (void) G_GNUC_CONST;
#define XFCE_TYPE_GRIP_STYLE (xfce_grip_style_get_type ())

G_END_DECLS

/* Generated data ends here */

