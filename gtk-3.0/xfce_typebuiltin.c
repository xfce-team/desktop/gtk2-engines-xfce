
/* Generated data (by glib-mkenums) */

#include <gtk/gtk.h>
#include "xfce_style_types.h"
#include "xfce_typebuiltin.h"

/* enumerations from "./xfce_style_types.h" */
GType xfce_grip_style_get_type(void)
{
    static GType etype = 0;
    if (G_UNLIKELY(etype == 0))
    {
        static const GEnumValue values[] = {
            { XFCE_GRIP_DISABLED, "XFCE_GRIP_DISABLED", "disabled" },
            { XFCE_GRIP_ROUGH, "XFCE_GRIP_ROUGH", "rough" },
            { XFCE_GRIP_SLIDE, "XFCE_GRIP_SLIDE", "slide" },
            { 0, NULL, NULL }
        };
        etype = g_enum_register_static(g_intern_static_string("XfceGripStyle"), values);
    }
    return etype;
}



/* Generated data ends here */

